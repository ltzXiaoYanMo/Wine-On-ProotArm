#!/usr/bin/bash
osCheck=$(uname -a)
if [[ $osCheck =~ 'arm64' ]] || [[ $osCheck =~ 'aarch64' ]];then
    architecture="arm64"
elif [[ $osCheck =~ 'armv7l' ]];then
    # shellcheck disable=SC2034
    architecture="armv7"
else
    echo "暂不支持的系统架构。"
    exit 1
fi
echo "HighStudios 2022~2023"
echo "WineArm Project v7.3"
echo "作者：high20212021"
echo "感谢这些人员为这个项目做出贡献！"
echo "Starduck"
echo "YanMo/ltzXiaoYanMo"
echo "=================="
echo "安装前预备"
echo "1.请勤更新脚本"
sleep 3
if ! command -v dialog &> /dev/null; then
    echo "脚本关键依赖项没有检查到：dialog，正在尝试安装"
    sudo apt-get -y install dialog
fi
if ! command -v tar &> /dev/null; then
    echo "脚本关键依赖项没有检查到：tar，正在尝试安装"
    sudo apt-get -y install tar
fi
if ! command -v axel &> /dev/null; then
    echo "脚本关键依赖项没有检查到：axel，正在尝试安装"
    sudo apt-get -y install axel
fi
if ! command -v git &> /dev/null; then
    echo "脚本关键依赖项没有检查到：git，正在尝试安装"
    sudo apt-get -y install git
fi
if ! command -v wget &> /dev/null; then
    echo "脚本关键依赖项没有检查到：wget，正在尝试安装"
    sudo apt-get -y install wget
fi
title="WineArm主菜单"
options=(
    1 "安装系统预备"
    2 "安装架构转译框架"
    3 "安装Wine本体"
    4 "备选方案安装Wine本体"
    5 "启动Wine-Arm"
    6 "备选方案启动WineArm"
    7 "更新脚本"
    8 "退出"
)
function function1() {
clear
sudo dpkg --add-architecture armhf
sudo apt-get update # 更新apt源
sudo apt-get upgrade
sudo apt-get install cmake make gcc clang git wget automake autoconf axel -y
sudo apt-get install zenity:armhf libegl-mesa0:armhf libgl1-mesa-dri:armhf libglapi-mesa:armhf libglx-mesa0:armhf libasound2:armhf libstdc++6:armhf libtcmalloc-minimal4:armhf gcc-arm-linux-gnueabihf sl:armhf -y
}
function build() {
clear
# git clone https://github.com/ptitSeb/box86
# git clone https://github.com/ptitSeb/box64
# git clone https://gitea.com/high20212021/box86-mirror
# git clone https://gitea.com/high20212021/box64-mirror
git clone https://git-proxy.ymbot.top/github.com/ptitSeb/box86.git
git clone https://git-proxy.ymbot.top/github.com/ptitSeb/box64.git
# 使用git-proxy clone项目
# shellcheck disable=SC2164
cd box86 || exit
mkdir build
cd build || exit
cmake .. -DNOGIT=1 -DCMAKE_BUILD_TYPE=RelWithDebInfo -DRPI4ARM64=ON
sudo make -j8
sudo make install
cd ../../box64 || exit
mkdir build
cd build || exit
cmake .. -DNOGIT=1 -DARM_DYNAREC=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo
sudo make -j8
sudo make install
}
function get_win() {
clear
cd ../..
axel -n 128 https://www.playonlinux.com/wine/binaries/phoenicis/upstream-linux-amd64/PlayOnLinux-wine-6.14-upstream-linux-amd64.tar.gz
sudo tar xvf PlayOnLinux-wine-6.14-upstream-linux-amd64.tar.gz -C /usr
rm PlayOnLinux-wine-6.14-upstream-linux-amd64.tar.gz
mkdir wine32
axel -n 128 https://www.playonlinux.com/wine/binaries/phoenicis/upstream-linux-x86/PlayOnLinux-wine-7.0-rc4-upstream-linux-x86.tar.gz
tar xvf PlayOnLinux-wine-7.0-rc4-upstream-linux-x86.tar.gz -C ~/wine32
rm PlayOnLinux-wine-7.0-rc4-upstream-linux-x86.tar.gz
cd /usr/bin || exit
ln -s ~/wine32/bin/wine wine32
cd ~/ || exit
}
function get_win_wget() {
clear
cd ../..
wget https://www.playonlinux.com/wine/binaries/phoenicis/upstream-linux-amd64/PlayOnLinux-wine-6.14-upstream-linux-amd64.tar.gz
sudo tar xvf PlayOnLinux-wine-6.14-upstream-linux-amd64.tar.gz -C /usr
rm PlayOnLinux-wine-6.14-upstream-linux-amd64.tar.gz
mkdir wine32
wget https://www.playonlinux.com/wine/binaries/phoenicis/upstream-linux-x86/PlayOnLinux-wine-7.0-rc4-upstream-linux-x86.tar.gz
tar xvf PlayOnLinux-wine-7.0-rc4-upstream-linux-x86.tar.gz -C ~/wine32
rm PlayOnLinux-wine-7.0-rc4-upstream-linux-x86.tar.gz
cd /usr/bin || exit
ln -s ~/wine32/bin/wine wine32
cd ~/ || exit
}
function update_scpt() {
    rm install_wine.sh
    echo 将从官方镜像源更新脚本
    axel -n 16 https://mirrors.ymbot.top/high20212021/install_wine.sh
    echo 更新完毕
}
function start_winexplorer() {
    clear
    echo "正在以备选方案启动WineArm"
    export DISPLAY=:0
    box64 wine64 explorer
}
function start_wine() {
    clear
    echo "请在图形界面检查状态"
    export DISPLAY=:0
    box64 wine64 taskmgr
}
while true; do
    choice=$(dialog --clear --menu "$title" 0 0 0 "${options[@]}" 2>&1 >/dev/tty)
    case $choice in
        1)
            function1
            ;;
        2)
            build
            ;;
        3)
            getwin
            ;;
        4)
            getwinwget
            ;;
        5)
            startwine
            ;;
        6)
            startwinexplore
            ;;
        7)
            updatescpt
            ;;
        8)
            break
            ;;
        *)
            echo "无效选项，请重新输入"
            ;;
    esac
done
