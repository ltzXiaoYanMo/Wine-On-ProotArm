# Wine-On-ProotArm
让你的Termux容器可以运行Windows应用！
如何使用：
首先安装一下它的前置依赖：
```Bash
sudo apt-get install wget axel git dialog
```
然后开始下载脚本：
```Bash
wget https://gitea.com/high20212021/Wine-On-ProotArm/raw/branch/main/install_wine.sh && bash install_wine.sh
```
然后按照步骤一步一步走即可
